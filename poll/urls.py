from django.urls import path
from . import views


urlpatterns = [
    path('', views.QuestionnaireView.as_view(), name='questionnaires'),
    path('questionnaires/<int:questionnaire_id>/', views.QuestionnaireDetailView.as_view(), name='questionnaire'),
    path('results/', views.ResultsView.as_view(), name='results'),
    path('api/questionnaires/', views.QuestionnaireListView.as_view(), name='questionnaires'),
    path('api/questions/', views.QuestionListView.as_view(), name='questions'),
    path('api/questions/<int:pk>/', views.QuestionView.as_view(), name='question'),

]
