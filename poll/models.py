from django.db import models
from django.contrib.auth.models import User
import json

# Create your models here.

class Questionnaire(models.Model):
    """Класс, описывающий опрос из нескольких вопросов"""
    name = models.CharField(max_length=100)
    pub_date = models.DateTimeField()
    end_date = models.DateTimeField()
    description = models.TextField()
    users = models.ManyToManyField(User, through='VotingResult')

class Question(models.Model):
    """Класс, описывающий отдельный вопрос опроса"""
    fields_type = (('t', 'text'), ('r', 'radio'), ('c', 'checkbox'))
    question_text = models.CharField(max_length=200, default='', blank=True)
    question_type = models.CharField(max_length=1, choices=fields_type, default='t')
    answer_options = models.JSONField(default=dict, blank=True)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)


class VotingResult(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    answer = models.JSONField(default=dict, blank=True)

