from django.shortcuts import render
from django.views.generic.base import View
from . models import Questionnaire, VotingResult, Question
from rest_framework.response import Response
from rest_framework.views import APIView
from . serializers import QuestionnaireListSerializer, QuestionSerializer
from . forms import *
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.urls import reverse
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly, AllowAny, IsAdminUser
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework.parsers import JSONParser
import json
from django.utils import timezone

# Create your views here.
class QuestionnaireView(View):
    """Список опросов, выводимый на страницу сайта"""
    #authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [AllowAny]
    def get(self, request):
        questionnaires = Questionnaire.objects.all()
        return render(request, "poll/questionnaire_list.html", {"questionnaire_list": questionnaires})

class QuestionnaireDetailView(View):
    """Страница опроса со списком вопросов"""
    #authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]
    def get_objects(self, questionnaire_id):
        try:
            questionnaire = Questionnaire.objects.get(pk=questionnaire_id)
            questions = questionnaire.question_set.all()
            return questionnaire, questions
        except Questionnaire.DoesNotExist:
            raise Http404

    def get(self, request, questionnaire_id):
        questionnaire, questions = self.get_objects(questionnaire_id)
        forms = []

        for question in questions:
            if question.question_type == 't':
                form = InputTypeForm(question_name=question.question_text)
            elif question.question_type == 'r':
                options = tuple([(name, name) for name in question.answer_options])
                form = RadioSelectForm(question_name=question.question_text, choices=options)
            elif question.question_type == 'c':
                options = tuple([(name, name) for name in question.answer_options])
                form = SelectForm(question_name=question.question_text, choices=options)
            forms.append(form)

        context = {"questionnaire": questionnaire, "questions": questions, "forms": forms}
        return render(request, "poll/questionnaire.html", context)

    def post(self, request, questionnaire_id):
        questionnaire, questions = self.get_objects(questionnaire_id)
        user = request.user

        answer = {}
        for question in questions:
            key = str(question.question_text)
            value = request.POST[key]
            answer.update({key: value})
        #answer = request.POST

        VotingResult.objects.create(user=user, questionnaire=questionnaire, answer=answer)
        return HttpResponseRedirect(reverse('results'))

class ResultsView(View):
    """Отображение списка результатов"""
    #authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticatedOrReadOnly]
    def get(self, request):
        user = request.user
        user_questionnaires = VotingResult.objects.filter(user=user)
        return render(request, 'poll/results.html', {'user_questionnaires': user_questionnaires})

class QuestionnaireListView(APIView):
    """Сериализация и десериализация списка опросов по API"""
    permission_classes = [IsAdminUser]
    def get(self, request):
        questionnaires = Questionnaire.objects.all()
        serializer = QuestionnaireListSerializer(questionnaires, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = QuestionnaireListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QuestionListView(APIView):
    """Сериализация и десериализация списка вопросов по API"""
    permission_classes = [IsAdminUser]
    def get(self, request):
        questions = Question.objects.all()
        serializer = QuestionSerializer(questions, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = QuestionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class QuestionView(APIView):
    """API для создания, изменения и удаления вопросов для опроса"""
    permission_classes = [IsAdminUser]
    def get_object(self, pk):
        try:
            return Question.objects.get(pk=pk)
        except Question.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        question = self.get_object(pk)
        serializer = QuestionSerializer(question)
        return Response(serializer.data)

    def put(self, request, pk):
        question = self.get_object(pk)
        serializer = QuestionSerializer(question, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_404_BAD_REQUEST)

    def delete(self, request, pk):
        question = self.get_object(pk)
        question.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)







