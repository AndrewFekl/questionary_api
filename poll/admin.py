from django.contrib import admin
from .models import Questionnaire, Question, VotingResult
from django.contrib.auth.models import User

# Register your models here.
admin.site.register(Question)
admin.site.register(Questionnaire)
admin.site.register(VotingResult)


