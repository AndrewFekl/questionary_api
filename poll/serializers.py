from rest_framework import serializers
from . models import Questionnaire, Question

class QuestionnaireListSerializer(serializers.ModelSerializer):
    """Сериализация списка опросов"""
    class Meta:
        model = Questionnaire
        fields = "__all__"

class QuestionSerializer(serializers.ModelSerializer):
    """Сериализация вопросов"""
    class Meta:
        model = Question
        fields = "__all__"


