from django import forms

class InputTypeForm(forms.Form):
    def __init__(self, question_name, *args, **kwargs):
        super(InputTypeForm, self).__init__(*args, **kwargs)
        self.fields[question_name] = forms.CharField(label=question_name, max_length=200)


class RadioSelectForm(forms.Form):

    def __init__(self, question_name, choices, *args, **kwargs):
        super(RadioSelectForm, self).__init__(*args, **kwargs)
        self.fields[question_name] = forms.ChoiceField(choices=choices, widget=forms.RadioSelect)

class SelectForm(forms.Form):

    def __init__(self, question_name, choices, *args, **kwargs):
        super(SelectForm, self).__init__(*args, **kwargs)
        self.fields[question_name] = forms.ChoiceField(choices=choices, widget=forms.CheckboxSelectMultiple)
